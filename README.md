# wasi-libc

Standard C library implementation for WebAssembly System Interface.

## Quick How-to

```
$ clang --target=wasm32-wasi --sysroot=/usr/wasm32-wasi -nodefaultlibs -lc -o hello hello.c
```

Important flags:

* `--target=wasm32-wasi` specifies the compilation architecture (WASM System Interface).
* `--sysroot=/usr/wasm32-wasi` tells clang where the library files are.
* `-nodefaultlibs` disables linking of libc and libcompiler-rt (the latter is not available).
* `-lc` re-enables linking of libc.
